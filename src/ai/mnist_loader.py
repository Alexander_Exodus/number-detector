import pickle
import gzip
import numpy as np
import random

def load_data():
	with gzip.open('data/mnist.pkl.gz', 'rb') as  f:
		training_data , validation_data , test_data = pickle.load(f, encoding='latin1')

	return (training_data , validation_data , test_data)



def load_data_wrapper():
	tr_d, va_d, te_d = load_data()

	training_inputs = [np.reshape(x, (784 , 1)) for x in tr_d[0]]
	training_results = [vectorized_result(y) for y in tr_d [1]]
	training_data = zip(training_inputs, training_results)

	validation_inputs = [np.reshape(x, (784, 1)) for x in va_d [0]]
	validation_data = zip(validation_inputs, va_d [1])

	test_inputs = [np.reshape(x, (784 , 1)) for x in te_d [0]]
	test_data = zip(test_inputs, te_d[1])
	index = random.randrange(0, len(tr_d[0])-1)
	example_data = np.reshape(tr_d[0][index], (28 , 28))
	return (training_data, validation_data , test_data, example_data)
# 'ab'
def vectorized_result(j):
	e = np.zeros((10 , 1))
	e[j] = 1.0
	return e

def wr(file, data):
	with open(file, 'wb') as f:
		pickle.dump(data, f)

def rd(file):
	with open(file, 'rb') as f:
		data = pickle.load(f)
	return data