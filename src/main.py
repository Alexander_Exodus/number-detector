import sys
from PySide2 import QtCore, QtGui, QtWidgets
from PySide2.QtCore import Qt, Slot
from ui_main import Ui_MainWindow
import numpy as np
import pickle


def rd(file):
    with open(file, 'rb') as f:
        data = pickle.load(f)
    return data


class Network():

    def __init__ (self, sizes):
        self.num_layers = len(sizes)
        self.sizes = sizes
        self.biases = [np.random.randn(y, 1) for y in sizes[1:]]
        self.weights = [np.random.randn(y, x) for x, y in zip(sizes[:-1], sizes[1:])]
        self.output_file = 'd'

    def sigmoid(self, x):
        return 1.0/(1.0 + np.exp(-x))


    def feedforward(self, a):
        for b, w in zip(self.biases, self.weights):
            a = self.sigmoid((w @ a)+b)
        return a

    def check_output(self, matrix):
        matrix = np.reshape(matrix, (784, 1))
        self.weights, self.biases = rd(self.output_file)
        return np.argmax(self.feedforward(matrix))


class Canvas(QtWidgets.QLabel):

    def __init__(self):
        super().__init__()
        pixmap = QtGui.QPixmap(588, 588)
        pixmap.fill(Qt.white)
        self.setPixmap(pixmap)

        self.last_x, self.last_y = None, None
        self.pen_color = QtGui.QColor('#000')        

    def set_pen_color(self, c):
        self.pen_color = QtGui.QColor(c)

    def mouseMoveEvent(self, e):
        if self.last_x is None: 
            self.last_x = e.x()
            self.last_y = e.y()
            return

        painter = QtGui.QPainter(self.pixmap())

        p = painter.pen()
        p.setWidth(30)
        p.setColor(self.pen_color)
        painter.setPen(p)
        painter.drawLine(self.last_x, self.last_y, e.x(), e.y())

        painter.end()
        
        self.update()
        self.last_x = e.x()
        self.last_y = e.y()

    def mouseReleaseEvent(self, e):
        self.last_x = None
        self.last_y = None


class MainWindow(QtWidgets.QMainWindow):

    def __init__(self):
        super().__init__()
        self.ui = Ui_MainWindow()
        self.ui.setupUi(self)

        self.canvas = Canvas()
        w = self.findChild(QtWidgets.QWidget, 'widget')

        l = QtWidgets.QVBoxLayout()
        l.setGeometry(QtCore.QRect(0, 0, 588, 588))
        w.setLayout(l)
        l.addWidget(self.canvas)

        check_button = self.findChild(QtWidgets.QPushButton, 'check')
        check_button.clicked.connect(self.save_as_matrix)

        clear_button = self.findChild(QtWidgets.QPushButton, 'clear')
        clear_button.clicked.connect(self.clear_window)

        self.label = self.findChild(QtWidgets.QLabel, 'label')


    @Slot()
    def save_as_matrix(self):
        temp  = self.canvas.pixmap().toImage().scaled(28, 28)
        matrix_w, matrix_h = temp.width(), temp.height()

        matrix = []
        for i in range(matrix_w):
            matrix.append([])
            for j in range(matrix_h):
                matrix[i].append(QtGui.QColor(temp.pixel(j,i)).black())
        matrix = [list(map(lambda x: x/255, i)) for i in matrix]

        net = Network([784, 30, 10])
        wynik = net.check_output(matrix)
        self.label.setText(str(wynik))


    @Slot()
    def clear_window(self):
        self.canvas.pixmap().fill(Qt.white)
        self.canvas.update()


app = QtWidgets.QApplication(sys.argv)
window = MainWindow()
window.show()
app.exec_()
